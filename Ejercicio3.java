package modulo1;

public class Ejercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
      System.out.println("tecla de escape\t\tSignificado");
      System.out.println("\\n\t\t\tSignifica nueva linea");
      System.out.println("\\t\t\t\tSignifica un tab de espacio");
      System.out.println("\\\"\t\t\tEs para poner comillas dobles dentro del texto por ejemplo");
      System.out.println("\t\t\tBelencita");
      System.out.println("\\\\\t\t\tSe utiliza para escribirla dentro del texto");
      System.out.println("\\'\t\t\tSe utiliza para las comillas simples para escribir por ejemplo 'Princesita' ");
	}

}
